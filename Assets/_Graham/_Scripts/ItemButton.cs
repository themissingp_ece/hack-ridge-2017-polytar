﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ItemButton : Damageable
{
    public string sceneName;
    public override void Kill()
    {
        SceneManager.LoadScene(sceneName);
    }
}
