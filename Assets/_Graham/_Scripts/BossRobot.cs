﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossRobot : Damageable
{
    public GameObject gibs;

    public float existenceLength = 60;
    private float timer = 0;
    private bool miss = false;

    void Update()
    {
        if (timer >= existenceLength && !miss)
        {
            miss = true;
            Instantiate(gibs, transform.position, transform.rotation);
            Destroy(gameObject);
        }
        timer += Time.deltaTime;
    }

    public override void Kill()
    {
        Instantiate(gibs, transform.position, transform.rotation);
        Score.score += 5;
        Destroy(gameObject);
    }
}
