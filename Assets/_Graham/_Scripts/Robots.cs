﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Robots : Damageable
{
    public GameObject parent;
    public GameObject hull;
    public GameObject gibs, teleGibs;

    public float existenceLength = 10;
    private float timer = 0;
    private bool miss = false;

    void Update()
    {
        if(timer >= existenceLength && !miss)
        {
            miss = true;
            Instantiate(teleGibs, transform.position, transform.rotation);
            Destroy(parent);
        }
        timer += Time.deltaTime;
        hull.transform.position = gameObject.transform.position;
    }

    public override void Kill()
    {
        Instantiate(gibs, transform.position, transform.rotation);
        Score.score += 1;
        Destroy(parent);
    }
}
