﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Damageable : MonoBehaviour
{
    public float stat_maxHealth, stat_currentHealth;

    public virtual void Damage(float damage)
    {
        stat_currentHealth -= damage;
        if(stat_currentHealth <= 0)
        {
            Kill();
        }
    }

    public virtual void Heal(float heal)
    {
        stat_currentHealth += heal;
        if(stat_currentHealth > stat_maxHealth)
        {
            stat_currentHealth = stat_maxHealth;
        }
    }

    public abstract void Kill();
}
