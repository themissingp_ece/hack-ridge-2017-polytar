﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageLink : Damageable
{
    public Damageable damageable;

	public override void Damage(float damage)
    {
        damageable.Damage(damage);
    }

    public override void Heal(float heal)
    {
        damageable.Heal(heal);
    }

    public override void Kill()
    {
        damageable.Kill();
    }
}
