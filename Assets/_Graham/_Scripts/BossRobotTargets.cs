﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossRobotTargets : Damageable
{
    public BossRobot parentBoss;
    public GameObject gibs;

    public override void Kill()
    {
        Instantiate(gibs, transform.position, transform.rotation);
        parentBoss.Damage(1);
        Score.score += 1;
        Destroy(gameObject);
    }
}
