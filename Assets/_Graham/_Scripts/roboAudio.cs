﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class roboAudio : MonoBehaviour
{
    public Rigidbody roboRigid;
    public AudioSource audioSource;
    public AudioClip flightLoop;
    public AudioClip[] robotPhysicsNoises;

    void FixedUpdate()
    {
        audioSource.pitch = Mathf.Lerp(0.8f, 1 + roboRigid.velocity.magnitude / 50, 0.5f);
    }

    void OnCollisionEnter(Collision other)
    {
        audioSource.PlayOneShot(robotPhysicsNoises[Random.Range(0, robotPhysicsNoises.Length)], other.relativeVelocity.magnitude / 5);
    }
}
