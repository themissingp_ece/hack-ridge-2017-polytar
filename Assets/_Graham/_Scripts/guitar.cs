﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class guitar : MonoBehaviour
{
    public AudioSource audioSource, laserAudio;
    public float pressDistance;
    public GameObject[] buttons;
    public bool[] buttonsOn;
    public AudioClip[] buttonClicks;
    public GameObject[] lasers;
    public GameObject raycastOrigin;

    public float damage;

    public bool firing = false, buttonPressed = false;

    void Start()
    {

    }


    void Update()
    {
        float temp = Input.GetAxis("GuitarStrum");
        if (temp > 0 || temp < 0)
        {
            firing = true;
        }
        else
        {
            firing = false;
        }

        if(Input.GetKeyDown(KeyCode.Joystick1Button1))
        {
            buttons[2].transform.localPosition = new Vector3(buttons[2].transform.localPosition.x, buttons[2].transform.localPosition.y, buttons[2].transform.localPosition.z - pressDistance);
            audioSource.PlayOneShot(buttonClicks[2], 1);
        }
        if (Input.GetKey(KeyCode.Joystick1Button1))
        {
            if(!buttonPressed)
            {
                if(firing)
                {
                    lasers[2].SetActive(true);
                }
                
                buttonPressed = true;
                buttonsOn[2] = true;
            }
            else if(buttonsOn[2])
            {
                lasers[2].SetActive(firing);
            }
        }
        else if (Input.GetKeyUp(KeyCode.Joystick1Button1))
        {
            buttons[2].transform.localPosition = new Vector3(buttons[2].transform.localPosition.x, buttons[2].transform.localPosition.y, 0.03149942f);
            if(buttonsOn[2])
            {
                lasers[2].SetActive(false);
                buttonsOn[2] = false;
                buttonPressed = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.Joystick1Button0))
        {
            buttons[1].transform.localPosition = new Vector3(buttons[1].transform.localPosition.x, buttons[1].transform.localPosition.y, buttons[1].transform.localPosition.z - pressDistance);
            audioSource.PlayOneShot(buttonClicks[1], 1);

        }
        if (Input.GetKey(KeyCode.Joystick1Button0))
        {
            if (!buttonPressed)
            {
                if (firing)
                {
                    lasers[1].SetActive(true);
                }
                buttonPressed = true;
                buttonsOn[1] = true;
            }
            else if (buttonsOn[1])
            {
                lasers[1].SetActive(firing);
            }
        }
        else if (Input.GetKeyUp(KeyCode.Joystick1Button0))
        {
            buttons[1].transform.localPosition = new Vector3(buttons[1].transform.localPosition.x, buttons[1].transform.localPosition.y, 0.03149942f);
            if (buttonsOn[1])
            {
                lasers[1].SetActive(false);
                buttonsOn[1] = false;
                buttonPressed = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.Joystick1Button3))
        {
            buttons[3].transform.localPosition = new Vector3(buttons[3].transform.localPosition.x, buttons[3].transform.localPosition.y, buttons[3].transform.localPosition.z - pressDistance);
            audioSource.PlayOneShot(buttonClicks[3], 1);

        }
        if (Input.GetKey(KeyCode.Joystick1Button3))
        {
            if (!buttonPressed)
            {
                if (firing)
                {
                    lasers[3].SetActive(true);
                }
                buttonPressed = true;
                buttonsOn[3] = true;
            }
            else if (buttonsOn[3])
            {
                lasers[3].SetActive(firing);
            }
        }
        else if (Input.GetKeyUp(KeyCode.Joystick1Button3))
        {
            buttons[3].transform.localPosition = new Vector3(buttons[3].transform.localPosition.x, buttons[3].transform.localPosition.y, 0.03149942f);
            if (buttonsOn[3])
            {
                lasers[3].SetActive(false);
                buttonsOn[3] = false;
                buttonPressed = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.Joystick1Button2))
        {
            buttons[4].transform.localPosition = new Vector3(buttons[4].transform.localPosition.x, buttons[4].transform.localPosition.y, buttons[4].transform.localPosition.z - pressDistance);
            audioSource.PlayOneShot(buttonClicks[4], 1);

        }
        if (Input.GetKey(KeyCode.Joystick1Button2))
        {
            if (!buttonPressed)
            {
                if (firing)
                {
                    lasers[4].SetActive(true);
                }
                buttonPressed = true;
                buttonsOn[4] = true;
            }
            else if (buttonsOn[4])
            {
                lasers[4].SetActive(firing);
            }
        }
        else if (Input.GetKeyUp(KeyCode.Joystick1Button2))
        {
            buttons[4].transform.localPosition = new Vector3(buttons[4].transform.localPosition.x, buttons[4].transform.localPosition.y, 0.03149942f);
            if (buttonsOn[4])
            {
                lasers[4].SetActive(false);
                buttonsOn[4] = false;
                buttonPressed = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.Joystick1Button4))
        {
            buttons[0].transform.localPosition = new Vector3(buttons[0].transform.localPosition.x, buttons[0].transform.localPosition.y, buttons[0].transform.localPosition.z - pressDistance);
            audioSource.PlayOneShot(buttonClicks[0], 1);

        }
        if (Input.GetKey(KeyCode.Joystick1Button4))
        {
            if (!buttonPressed)
            {
                if (firing)
                {
                    lasers[0].SetActive(true);
                }
                buttonPressed = true;
                buttonsOn[0] = true;
            }
            else if (buttonsOn[0])
            {

                lasers[0].SetActive(firing);
 
            }
        }
        else if (Input.GetKeyUp(KeyCode.Joystick1Button4))
        {
            buttons[0].transform.localPosition = new Vector3(buttons[0].transform.localPosition.x, buttons[0].transform.localPosition.y, 0.03149942f);
            if (buttonsOn[0])
            {
                lasers[0].SetActive(false);
                buttonsOn[0] = false;
                buttonPressed = false;
            }
        }
    }
}
