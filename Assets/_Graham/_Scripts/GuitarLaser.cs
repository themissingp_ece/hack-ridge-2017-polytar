﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuitarLaser : MonoBehaviour
{
    public guitar guit;

    void OnTriggerEnter(Collider other)
    {
        col(other);
    }

    void OnTriggerStay(Collider other)
    {
        col(other);
    }

    void OnTriggerLeave(Collider other)
    {
        col(other);
    }

    void col(Collider other)
    {
        Damageable dam = other.GetComponent<Damageable>();
        if (guit.firing && dam != null)
        {
            if (guit.buttonsOn[0] && other.CompareTag("Orange"))
            {
                dam.Damage(1);
            }
            else if (guit.buttonsOn[1] && other.CompareTag("Green"))
            {
                dam.Damage(1);
            }
            else if (guit.buttonsOn[2] && other.CompareTag("Red"))
            {
                dam.Damage(1);
            }
            else if (guit.buttonsOn[3] && other.CompareTag("Yellow"))
            {
                dam.Damage(1);
            }
            else if (guit.buttonsOn[4] && other.CompareTag("Blue"))
            {
                dam.Damage(1);
            }
        }
    }
}
