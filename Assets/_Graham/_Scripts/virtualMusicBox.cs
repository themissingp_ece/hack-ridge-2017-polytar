﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class virtualMusicBox : MonoBehaviour
{
    public string menuSceneName;
    public List<Vector4> musicEvents = new List<Vector4>();
    public float timer;
    public GameObject[] Robos = new GameObject[5];
    public GameObject bossRobo;
    public Transform spawnArea, bossSpawnArea;
    public float spawnLocMod;
    public AudioSource metronome;

	void Start ()
    {
		
	}

	void FixedUpdate ()
    {
        timer += Time.fixedDeltaTime;
        if(musicEvents.Count > 0)
        {
            checkMusicNote();
        }
	}

    void checkMusicNote()
    {
        if((musicEvents[0].x * 60) + musicEvents[0].y + (musicEvents[0].z / 100f) <= timer)
        {
            metronome.Play();
            if(musicEvents[0].w >= 99 && musicEvents[0].w <= 150)
            {
                Instantiate(bossRobo, bossSpawnArea.position, new Quaternion());
            }
            else if(musicEvents[0].w >= 499)
            {
                SceneManager.LoadScene(menuSceneName);
            }
            else
            {
                for(int i = 0; i <musicEvents[0].w; i++)
                {
                    Instantiate(Robos[Random.Range(0, Robos.Length)], new Vector3(spawnArea.position.x + Random.Range(-spawnLocMod, spawnLocMod), spawnArea.position.y + Random.Range(-spawnLocMod, spawnLocMod), spawnArea.position.z + Random.Range(-spawnLocMod, spawnLocMod)), new Quaternion());
                }
            }
            musicEvents.RemoveAt(0);
            if (musicEvents.Count > 0)
            {
                checkMusicNote();
            }
        }
    }
}
