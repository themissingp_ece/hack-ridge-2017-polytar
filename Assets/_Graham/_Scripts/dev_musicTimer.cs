﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dev_musicTimer : MonoBehaviour
{
    public float internalTicker, internalTotalTime;
    public float songTempo;
    public float set = 0, internalSetTime = 0;
    public AudioSource audioSource;
    public AudioClip beep;
    private float songTempoCode;

    public AudioSource music;

    public bool ticking = false;

	void Start ()
    {
        songTempoCode = 60 / songTempo;
        internalTicker = songTempoCode + 1;
        StartSong();
	}
	
    void StartSong()
    {
        music.Play();
        ticking = true;
    }

	void FixedUpdate ()
    {
        if(ticking)
        {
            internalTicker += Time.fixedDeltaTime;
            internalTotalTime += Time.fixedDeltaTime;

            if (internalTicker >= songTempoCode)
            {
                internalTicker = 0;
                audioSource.PlayOneShot(beep, 1f);
                internalSetTime++;
                if (internalSetTime >= 4)
                {
                    internalSetTime = 0;
                    set++;
                }
            }
        }
	}
}
