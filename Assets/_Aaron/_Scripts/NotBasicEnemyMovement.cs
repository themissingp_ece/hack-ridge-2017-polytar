﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotBasicEnemyMovement : BasicEnemyMovement{
    public float slowSpeed;
    public float close;
    public float spinTorque;
    public float upward;
	void Start () {
		
	}

	void FixedUpdate () {
        speedLimit(maxVelocity);
        aproach();
        doNotLeave(25, 15, upward);
        spin(spinTorque);
    }
    void aproach()
    {
        if (enemy.transform.position.z<= close)
        {
            enemy.AddForce(forward * slowSpeed);
        }
        else
        {
            enemy.AddForce(backward * slowSpeed);
        }
    }
}
