﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEnemyMovement : MonoBehaviour {
    public Rigidbody enemy;
    private Rigidbody player;
    private Transform player1;
    Vector3 playerrel;
    public float maxVelocity = 27.0f;
    public int speed = 3;
    public int safespace = 3;
    private float rand;
    public Vector3 left = new Vector3(-2, 0, 0);
    public Vector3 right = new Vector3(2, 0, 0);
    public Vector3 forward = new Vector3(0, 0, 2);
    public Vector3 backward = new Vector3(0, 0, -2);
    public Vector3 up = new Vector3(0, 2.1f, 0);
    public Vector3 down = new Vector3(0, -1, 0);
    void Start () {
    
    }
	void FixedUpdate ()
    {
        enemy.AddExplosionForce(5, enemy.transform.position, 2);
        spin();
        speedLimit(maxVelocity);
        //getClose();
        getCloseGlobal();
        //stayBack();
        stayBackGlobal();
        doNotLeave(6, 3.2f, speed);
    }
    public void getCloseGlobal()
    {
        if (enemy.transform.position.z >= 0)
        {
            enemy.AddForce(backward * speed * 2);
            //print("Backward");
        }
        if (enemy.transform.position.z <= -safespace)
        {
            enemy.AddForce(forward * speed);
            //print("Forward");
        }
        if (enemy.transform.position.x >= safespace)
        {
            enemy.AddForce(left * speed);
            //print("Left");
        }
        if (enemy.transform.position.x <= -safespace)
        {
            enemy.AddForce(right * speed);
            //print("Right");
        }
    }
    public void getCloseGlobal(float x, float y, float z)
    {
        if (enemy.transform.position.z >= 0)
        {
            enemy.AddForce(backward * speed * 2);
            //print("Backward");
        }
        if (enemy.transform.position.z <= -safespace)
        {
            enemy.AddForce(forward * speed);
            //print("Forward");
        }
        if (enemy.transform.position.x >= safespace)
        {
            enemy.AddForce(left * speed);
            //print("Left");
        }
        if (enemy.transform.position.x <= -safespace)
        {
            enemy.AddForce(right * speed);
            //print("Right");
        }
    }
    public void stayBackGlobal()
    {
        if(enemy.transform.position.z >= -5)
        {
            enemy.AddForce(backward * speed);
        }
        if (enemy.transform.position.z <= safespace/3)
        {
            if (enemy.transform.position.z >= 0)
            {
                enemy.AddForce(forward * speed * 2);
            }
            
            //print("Backward");
        }
        if (enemy.transform.position.z >= -safespace/3)
        {
            if (enemy.transform.position.z <= 0)
            {
                enemy.AddForce(backward * speed * 2);
            }
            
            //print("Forward");
        }
        if (enemy.transform.position.x <= safespace/3)
        {
            if (enemy.transform.position.x >= 0)
                if(enemy.transform.position.z >= -safespace)
                    enemy.AddForce(right * speed * 1.2f);
            //print("Left");
        }
        if (enemy.transform.position.x >= -safespace/3)
        {
            if (enemy.transform.position.x <= 0)
                if(enemy.transform.position.z >= -safespace)
                    enemy.AddForce(left * speed * 1.2f);
            //print("Right");
        }
    }
    public void stayBackGlobal(float x, float y, float z)
    {
        if (enemy.transform.position.z >= -y)
        {
            enemy.AddForce(backward * speed);
        }
        if (enemy.transform.position.z <= z / 3)
        {
            if (enemy.transform.position.z >= 0)
            {
                enemy.AddForce(forward * speed * 2);
            }

            //print("Backward");
        }
        if (enemy.transform.position.z >= -z / 3)
        {
            if (enemy.transform.position.z <= 0)
            {
                enemy.AddForce(backward * speed * 2);
            }

            //print("Forward");
        }
        if (enemy.transform.position.x <= x / 3)
        {
            if (enemy.transform.position.x >= 0)
                if (enemy.transform.position.z >= -z)
                    enemy.AddForce(right * speed * 1.2f);
            //print("Left");
        }
        if (enemy.transform.position.x >= -x / 3)
        {
            if (enemy.transform.position.x <= 0)
                if (enemy.transform.position.z >= -z)
                    enemy.AddForce(left * speed * 1.2f);
            //print("Right");
        }
    }
    public void speedLimit(float x)
    {
        if (enemy.velocity.magnitude > x)
        {
            enemy.velocity = enemy.velocity.normalized * x;
        }
    }
    public void spin()
    {
        rand = (Random.value * 100);
        if (rand <= 2)
        {
            enemy.AddTorque(10, 0, 0);
        }
        else if (rand <= 4)
        {
            enemy.AddTorque(0, 10, 0);
        }
        else if (rand <= 6)
        {
            enemy.AddTorque(0, 0, 10);
        }
        else if (rand <= 8)
        {
            enemy.AddTorque(-10, 0, 0);
        }
        else if (rand <= 10)
        {
            enemy.AddTorque(0, -10, 0);
        }
        else if (rand <= 12)
        {
            enemy.AddTorque(0, 0, -10);
        }
    }
    public void spin(float y)
    {
        enemy.AddTorque(0, y, 0);
    }
    public void doNotLeave(float max, float min, float strength)
    {
        if (enemy.transform.position.y >= max)
        {
            enemy.AddForce(down * strength);
            //print("Down");
        }
        if (enemy.transform.position.y <= min)
        {
            enemy.AddForce(up * strength);
            //print("up");
        }
        if (enemy.transform.position.y <= 0)
        {
            enemy.AddForce(up * strength * 3);
        }
    }

    /// <summary>
    // Useless Stuff I might nee
    /// </summary>
    void stuff()
    {
        player1 = player.transform;
        playerrel = player1.InverseTransformPoint(transform.position);
    }
    void getClose()
    {
        if (playerrel.z >= safespace)
        {
            enemy.AddForce(backward * speed);
            //print("Backward");
        }
        if (playerrel.z <= -safespace)
        {
            enemy.AddForce(forward * speed);
            //print("Forward");
        }
        if (playerrel.x >= safespace)
        {
            enemy.AddForce(left * speed);
            //print("Left");
        }
        if (playerrel.x <= -safespace)
        {
            enemy.AddForce(right * speed);
            //print("Right");
        }
    }
    void stayBack()
    {
        if (playerrel.z <= safespace / 3)
        {
            if (playerrel.z >= 0)
            {
                enemy.AddForce(forward * speed * 2);
            }

            //print("Backward");
        }
        if (playerrel.z >= -safespace / 3)
        {
            if (playerrel.z <= 0)
            {
                enemy.AddForce(backward * speed * 2);
            }

            //print("Forward");
        }
        if (playerrel.x <= safespace / 3)
        {
            if (playerrel.x >= 0)
                enemy.AddForce(right * speed * 2);
            //print("Left");
        }
        if (playerrel.x >= -safespace / 3)
        {
            if (playerrel.x <= 0)
                enemy.AddForce(left * speed * 2);
            //print("Right");
        }
    }
}
