﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(virtualMusicBox))]
public class virtualMusicBoxEditor : Editor
{
    public override void OnInspectorGUI()
    {
        virtualMusicBox myTarget = (virtualMusicBox)target;

        DrawDefaultInspector();

        EditorGUILayout.LabelField("  -| Note Paper |-");

        for(int i = 0; i < myTarget.musicEvents.Count; i++)
        {
            myTarget.musicEvents[i] = EditorGUILayout.Vector4Field("Event " + i, myTarget.musicEvents[i]);
        }
        if(GUILayout.Button("+ Add Event Slot"))
        {
            myTarget.musicEvents.Add(new Vector4());
        }
        if (GUILayout.Button("- Remove Event Slot"))
        {
            myTarget.musicEvents.RemoveAt(myTarget.musicEvents.Count - 1);
        }
    }
}
