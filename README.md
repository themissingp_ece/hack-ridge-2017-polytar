# POLYTAR #


## Inspiration

Virtual Reality has become a huge rising technology, and we were interested in seeing how different input options would effect the overall game-play experience and immersion of a project. A Rock-band guitar was selected because it was something that we all had, and was easy to work with.

## What it does

Polytar uses a rock-band guitar in tandem with an HTC Vive controller to create a virtual guitar that shoots lasers. In Polytar, you can choose between a few pre-selected songs to play along to while destroying as many robots as you can.

## How we built it

We designed and 3D printed a mount for the Vive controller before-hand so we could work with the guitar. After that, we used a wireless Xbox controller adapter to recognize it's input in Unity. From there, we split up the work between designing the AI, Sound Design, 3D Modeling, and Game Functionality. For creating levels, we made a basic level editor that lets you schedule when and how many enemies are to spawn.

## Challenges we ran into

Originally, Polytar was planned to be a multiplayer game. We learned after the first 5 hours of Hack//Ridge that we probably wouldn't be able to fit that in within 24 hours, especially for such a physics heavy game. We decided to abandon multiplayer support in favor of getting started on the actual game. It also was initially planned to have midi support, letting you play any song as long as it had the appropriate midi file. It turns out that the library we planned on using to access and read MIDI files had next to no documentation or support, so we quickly dropped that as well, and came up with our own solution. 

## Accomplishments that we're proud of

We're proud of being able to pull off our game in 24 hours. After wasting at least 6 hours on features that we could never get to work, we were worried about being able to pull through in time. We managed to finish our game with an hour to spare, which made us very proud.

## What we learned

Scope is a very important factor in any project. We dreamed up this perfect view of a coop VR rhythm game without thinking of the time we had to complete it. We also certainly learned a lot about handling a situation when it doesn't go as planned.

## What's next for Polytar

Outside of the hackathon, Polytar may *eventually* get midi support, but with college plans around the bend, we may have to focus on other things. It would be ideal to get Polytar to a state where it can be downloaded or sold online on a site like itch.io, but that would involve either greater user creation tools or an algorithm to automatically determine spawn rates for a song.